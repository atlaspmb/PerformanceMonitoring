#!/bin/bash

# Define and execute the main job
execute() {

    # Define test parameters
    JOBNAME="${1}";
    JOBRELEASE="${2}";
    JOBPLATFORM="${3}";
    

    # Define the top-level workdir
    WORKDIR="/data/atlaspmb/athenamt-perfmonmt-jobs/vtune/hotspot";
    lsetup "views LCG_104b ${JOBPLATFORM}"
    # Create the rundir
    RUNDIR="${WORKDIR}/${JOBNAME}/${JOBRELEASE}/${JOBPLATFORM}";
    if [[ ! -d ${RUNDIR} ]]; then
        echo "Directory ${RUNDIR} doesn't exist, nothing to do...";
        return 0;
    fi
    cd ${RUNDIR};
    if [[ ! -d "${HOME}/www_atlaspmb/athenamt-${JOBNAME}-vtune-reports" ]]; then

        mkdir -p ${HOME}/www_atlaspmb/athenamt-${JOBNAME}-vtune-reports;

    fi
    nightles_=($(find . -name "*T*" -type d -printf "%f\n"));
    i=0;
    declare -a  nightles;
    for nightly in "${nightles_[@]}"
    do
     if [[ ! -d "${HOME}/www_atlaspmb/athenamt-${JOBNAME}-vtune-reports/${nightly}"  ]]; then
        nightles[$i]="${nightly}"
        i=$((i+1));
     fi
    done


   for nightly in "${nightles[@]}"
   do

      cd "${nightly}";

      mkdir -p ${HOME}/www_atlaspmb/athenamt-${JOBNAME}-vtune-reports/${nightly};
      vtune -report gprof-cc -result-dir r000hs -format text -report-output ${HOME}/www_atlaspmb/athenamt-${JOBNAME}-vtune-reports/${nightly}/profile_${nightly}.txt;
    #  python /data/tania/vtune/vtunescripts/gprof2dot/gprof2dot.py -s --color-nodes-by-selftime -f axe reports/profile_${nightly}.txt | dot -Tpdf -o reports/figure_gprof2dot_${nightly}.pdf;

      vtune -report top-down -call-stack-mode all -column="CPU Time:Self","Module" -report-out ${HOME}/www_atlaspmb/athenamt-${JOBNAME}-vtune-reports/${nightly}/profile_top-down_${nightly}.csv -filter "Function Stack" -format csv -csv-delimiter comma;
      sed -i '/war:Column filter is ON./d' ${HOME}/www_atlaspmb/athenamt-${JOBNAME}-vtune-reports/${nightly}/profile_top-down_${nightly}.csv;
      perl /afs/cern.ch/atlas/project/pmb/spot/FlameGraph/stackcollapse-vtune.pl ${HOME}/www_atlaspmb/athenamt-${JOBNAME}-vtune-reports/${nightly}/profile_top-down_${nightly}.csv| perl /afs/cern.ch/atlas/project/pmb/spot/FlameGraph/flamegraph.pl > ${HOME}/www_atlaspmb/athenamt-${JOBNAME}-vtune-reports/${nightly}/profile_top-down_${nightly}.svg;
      # Top 20 hotspots
      vtune -report summary -format text -report-output ${HOME}/www_atlaspmb/athenamt-${JOBNAME}-vtune-reports/${nightly}/profile_summary_${nightly}.txt;
      vtune -report hotspots -limit 20 -column="CPU Time:Self,Module" -report-width 150 -format text -report-output ${HOME}/www_atlaspmb/athenamt-${JOBNAME}-vtune-reports/${nightly}/profile_hotspots_${nightly}.txt;

      # Text report
      echo "*******************************************************************************" > ${HOME}/www_atlaspmb/athenamt-${JOBNAME}-vtune-reports/${nightly}/profile_final_${nightly}.txt
      echo "                                   Summary" >> ${HOME}/www_atlaspmb/athenamt-${JOBNAME}-vtune-reports/${nightly}/profile_final_${nightly}.txt
      echo "*******************************************************************************" >> ${HOME}/www_atlaspmb/athenamt-${JOBNAME}-vtune-reports/${nightly}/profile_final_${nightly}.txt
      cat ${HOME}/www_atlaspmb/athenamt-${JOBNAME}-vtune-reports/${nightly}/profile_summary_${nightly}.txt >> ${HOME}/www_atlaspmb/athenamt-${JOBNAME}-vtune-reports/${nightly}/profile_final_${nightly}.txt
      echo "" >> ${HOME}/www_atlaspmb/athenamt-${JOBNAME}-vtune-reports/${nightly}/profile_final_${nightly}.txt
      echo "*******************************************************************************" >> ${HOME}/www_atlaspmb/athenamt-${JOBNAME}-vtune-reports/${nightly}/profile_final_${nightly}.txt
      echo "                              Top 20 Consumers" >> ${HOME}/www_atlaspmb/athenamt-${JOBNAME}-vtune-reports/${nightly}/profile_final_${nightly}.txt
      echo "*******************************************************************************" >> ${HOME}/www_atlaspmb/athenamt-${JOBNAME}-vtune-reports/${nightly}/profile_final_${nightly}.txt
      cat ${HOME}/www_atlaspmb/athenamt-${JOBNAME}-vtune-reports/${nightly}/profile_hotspots_${nightly}.txt >> ${HOME}/www_atlaspmb/athenamt-${JOBNAME}-vtune-reports/${nightly}/profile_final_${nightly}.txt

      cd ..;
  done
}

# Define the main function
main() {

    # Setup environment

    source ~/.bashrc;
    source ~/.bash_profile;
    source /cvmfs/projects.cern.ch/intelsw/oneAPI/linux/all-setup.sh;

    # Define the jobs this setup will attempt to run
    JOBS=( "main x86_64-el9-gcc13-opt rawtoall_data23_mt8" \
           "main x86_64-el9-gcc13-opt ttbar_fullsim_mt8" )

    # Loop over all defined jobs
    for job in "${JOBS[@]}"
    do
        # Parse the inputs: release platform name
        vals=(${job})
        jobrelease="${vals[0]}"
        jobplatform="${vals[1]}"
        jobname="${vals[2]}"
        # Process the job
        execute "${jobname}" "${jobrelease}" "${jobplatform}"
    done

}

# Execute main
main
