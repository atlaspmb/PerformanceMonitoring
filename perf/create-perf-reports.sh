#!/bin/bash

# Define and execute the main job
execute() {

    # Define test parameters
    JOBNAME="${1}";
    JOBRELEASE="${2}";
    JOBPLATFORM="${3}";
 

    # Define the top-level workdir
    BASEDIR="/afs/cern.ch/atlas/project/pmb/new_pmb/PerformanceMonitoring";
    WORKDIR="/data/atlaspmb/athenamt-perfmonmt-jobs/perf";
    lsetup "views LCG_104b ${JOBPLATFORM}"
    # Create the rundir
    RUNDIR="${WORKDIR}/${JOBNAME}/${JOBRELEASE}/${JOBPLATFORM}";
    if [[ ! -d ${RUNDIR} ]]; then
        echo "Directory ${RUNDIR} doesn't exist, nothing to do...";
        return 0;
    fi
    cd ${RUNDIR};
    if [[ ! -d "${HOME}/www_atlaspmb/athenamt-${JOBNAME}-perf-reports" ]]; then

        mkdir -p ${HOME}/www_atlaspmb/athenamt-${JOBNAME}-perf-reports;

    fi
    nightles_=($(find . -name "*T*" -type d -printf "%f\n"));
    i=0;
    declare -a  nightles;
    for nightly in "${nightles_[@]}"
    do
     if [[ ! -d "${HOME}/www_atlaspmb/athenamt-${JOBNAME}-perf-reports/${nightly}"  ]]; then
        nightles[$i]="${nightly}"
        i=$((i+1));
     fi
    done


   for nightly in "${nightles[@]}"
   do
      cd "${nightly}";
      ls .
      mkdir -p ${HOME}/www_atlaspmb/athenamt-${JOBNAME}-perf-reports/${nightly};

      perf report --stdio --fields overhead,comm,dso,symbol -i perf.data  2>&1 | grep -vE '^\s*$|^#(\s*\.+)+\s*$' | head -30 > ${HOME}/www_atlaspmb/athenamt-${JOBNAME}-perf-reports/${nightly}/perf_top_${nightly}.txt;
      python3 $BASEDIR/perf/undemangle.py -i ${HOME}/www_atlaspmb/athenamt-${JOBNAME}-perf-reports/${nightly}/perf_top_${nightly}.txt  -o ${HOME}/www_atlaspmb/athenamt-${JOBNAME}-perf-reports/${nightly}/perf_top_undemangled_${nightly}.txt;
      perf report --stdio --fields overhead,comm,time,dso,symbol -i perf.data  > ${HOME}/www_atlaspmb/athenamt-${JOBNAME}-perf-reports/${nightly}/perf_full_${nightly}.txt 2>&1;
      cp stat.txt ${HOME}/www_atlaspmb/athenamt-${JOBNAME}-perf-reports/${nightly}/perf_stat_${nightly}.txt;

      # Text report
      echo "*******************************************************************************" > ${HOME}/www_atlaspmb/athenamt-${JOBNAME}-perf-reports/${nightly}/profile_final_${nightly}.txt
      echo "                                   Summary" >> ${HOME}/www_atlaspmb/athenamt-${JOBNAME}-perf-reports/${nightly}/profile_final_${nightly}.txt;
      echo "*******************************************************************************" >> ${HOME}/www_atlaspmb/athenamt-${JOBNAME}-perf-reports/${nightly}/profile_final_${nightly}.txt;
      cat ${HOME}/www_atlaspmb/athenamt-${JOBNAME}-perf-reports/${nightly}/perf_stat_${nightly}.txt >> ${HOME}/www_atlaspmb/athenamt-${JOBNAME}-perf-reports/${nightly}/profile_final_${nightly}.txt;
      echo "" >> ${HOME}/www_atlaspmb/athenamt-${JOBNAME}-perf-reports/${nightly}/profile_final_${nightly}.txt;
      echo "*******************************************************************************" >> ${HOME}/www_atlaspmb/athenamt-${JOBNAME}-perf-reports/${nightly}/profile_final_${nightly}.txt;
      echo "                              Top 20 Consumers" >> ${HOME}/www_atlaspmb/athenamt-${JOBNAME}-perf-reports/${nightly}/profile_final_${nightly}.txt
      echo "*******************************************************************************" >> ${HOME}/www_atlaspmb/athenamt-${JOBNAME}-perf-reports/${nightly}/profile_final_${nightly}.txt;
      cat ${HOME}/www_atlaspmb/athenamt-${JOBNAME}-perf-reports/${nightly}/perf_top_undemangled_${nightly}.txt >> ${HOME}/www_atlaspmb/athenamt-${JOBNAME}-perf-reports/${nightly}/profile_final_${nightly}.txt;

      cd ..;
  done
}

# Define the main function
main() {

    # Setup environment

    source ~/.bashrc;
    source ~/.bash_profile;

    # Define the jobs this setup will attempt to run
    JOBS=( "main--dev4LCG x86_64-el9-gcc13-opt rawtoall_data23_mt8" \
           "main--dev4LCG x86_64-el9-gcc13-opt ttbar_fullsim_mt8"  )

    # Loop over all defined jobs
    for job in "${JOBS[@]}"
    do
        # Parse the inputs: release platform name
        vals=(${job})
        jobrelease="${vals[0]}"
        jobplatform="${vals[1]}"
        jobname="${vals[2]}"
        # Process the job
        execute "${jobname}" "${jobrelease}" "${jobplatform}"
    done

}

# Execute main
main
