#!/usr/bin/env python3

__author__ = "Alaettin Serhan Mete <amete@cern.ch>"
__version__ = "0.0.1"

from os import path, makedirs
import argparse
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import sqlalchemy as sa

from create_spot_html_snippet import create_html_snippet

markers = [ 'o', 's', '^', 'P', 'p', '<', '*',  'H', '>', 'X', 'D',
            '1', 'x', 'd', 'h', '8', 'v' ]
markers.extend(markers) # simply double the list...
markers.extend(markers) # simply double the list...

def adjust_ylim(ax, factor = 0.05):
    """ A basic function to readjust y-axis limits """

    # Read the current limits
    ymin, ymax = ax.get_ylim()

    # Calculate the new ones
    ymin *= (1-factor) if ymin > 0 else (1+factor)
    ymax *= (1+factor) if ymax > 0 else (1-factor)

    # Set the new ones
    ax.set_ylim((ymin,ymax))

def create_pointplot(**kwargs):
    """ A basic wrapper around seaborn's pointplot """

    # Plot
    fig, ax = plt.subplots()
    if 'invertedx' in kwargs and kwargs['invertedx']:
        sns.pointplot(**kwargs, markers=markers).invert_xaxis()
    else:
        sns.pointplot(**kwargs, markers=markers)

    # Set title
    if 'title' in kwargs:
        ax.set_title(kwargs['title'])

    # Set labels
    if 'xlabel' in kwargs:
        ax.set(xlabel = kwargs['xlabel'])
    if 'ylabel' in kwargs:
        ax.set(ylabel = kwargs['ylabel'])

    # Set limits
    if 'xlim' in kwargs:
        ax.set_xlim(kwargs['xlim'])
    if 'ylim' in kwargs:
        ax.set_ylim(kwargs['ylim'])
    else:
        adjust_ylim(ax)

    # Set legend
    leg = plt.legend(bbox_to_anchor = (1.01, 1.01),
                     loc = 'upper left',
                     prop={'size': '8'})
    leg.get_frame().set_linewidth(0)

    # Rotate xticks, set grid and tight layout
    plt.xticks(rotation=90)
    ax.grid()
    plt.tight_layout()

    # Save the file in the appropriate folder
    fname = kwargs['fname'] if 'fname' in kwargs else 'plot.png'
    dirname = kwargs['folder'] if 'folder' in kwargs else '.'
    if dirname and not path.exists(dirname):
        makedirs(dirname)
    plt.savefig(f"{dirname}/{fname}")
    plt.close()

def create_two_pad_plots(jobs, dirname, title, xlabel, yvars, ylabels, plotprefix, plotsuffix):
    """ A basic wrapper to create the summary plots for the overview page """

    # Check the inputs
    if len(yvars) != len(ylabels) or len(yvars) != 2 or not plotprefix or not plotsuffix:
        return

    # Create the plot
    fig, ax = plt.subplots(2, 1, sharex = True)
    sns.pointplot(ax = ax[0], data = jobs, x = 'nightly', y = yvars[0]).invert_xaxis()
    ax[0].set_title(title)
    ax[0].set(xlabel = None, ylabel = ylabels[0])
    adjust_ylim(ax[0])
    ax[0].grid()
    sns.pointplot(ax = ax[1], data = jobs, x = 'nightly', y = yvars[1]).invert_xaxis()
    ax[1].set(xlabel = xlabel, ylabel = ylabels[1])
    adjust_ylim(ax[1])
    ax[1].grid()
    plt.xticks(rotation=90)
    plt.tight_layout()
    plt.subplots_adjust(hspace = 0.15)
    plt.savefig(f"{dirname}/{plotprefix}-summary-{plotsuffix}.png")
    plt.close()

def create_summary_plots(jobs, dirname, xlabel, jobname, jobstep):
    """ Main function for creating the summary plots """

    # Create the output directory if necessary
    if not path.exists(dirname):
        makedirs(dirname)

    plotprefix = jobstep

    # CPU
    yvars = ['cpu_efficiency', 'cpu_per_event']
    ylabels = ['CPU Efficiency', 'CPU/Event [s]']
    title = f"CPU Utilization\nJob: {jobname}"
    plotsuffix = 'cpu'
    create_two_pad_plots(jobs, dirname, title, xlabel, yvars, ylabels, plotprefix, plotsuffix)

    # Throughput
    yvars = ['throughput', 'wall_per_event']
    ylabels = ['Events [1/s]', 'Wall/Event [s]']
    title = f"Throughput\nJob: {jobname}"
    plotsuffix = 'throughput'
    create_two_pad_plots(jobs, dirname, title, xlabel, yvars, ylabels, plotprefix, plotsuffix)

    # Memory - 1
    yvars = ['vmem_per_thread', 'pss_per_thread']
    ylabels = ['VMEM', 'PSS']
    title = f"Maximum Memory [MB/thread]\nJob: {jobname}"
    plotsuffix = 'memory'
    create_two_pad_plots(jobs, dirname, title, xlabel, yvars, ylabels, plotprefix, plotsuffix)

    # Memory - 2
    yvars = ['leak_estimate_vmem', 'leak_estimate_pss']
    ylabels = ['VMEM', 'PSS']
    title = f"Memory Slopes [KB/Event]\nJob: {jobname}"
    plotsuffix = 'memory-leak'
    create_two_pad_plots(jobs, dirname, title, xlabel, yvars, ylabels, plotprefix, plotsuffix)

    # Nthreads/slots
    yvars = ['nthreads', 'nslots']
    ylabels = ['Threads', 'Slots']
    title = f"Number of threads/slots\nJob: {jobname}"
    plotsuffix = 'threads-slots'
    create_two_pad_plots(jobs, dirname, title, xlabel, yvars, ylabels, plotprefix, plotsuffix)

def main():
    """ Main function """

    # Parse the user inputs
    parser = argparse.ArgumentParser(description = 'Script to create plots for the daily SPOT monitoring webpages')

    parser.add_argument('-i', '--input', type = str, required = True,
                        help = 'Input SQL DB file')
    parser.add_argument('-o', '--outdir', type = str, required = True,
                        help = 'Output directory for the plots')
    parser.add_argument('-j', '--jobname', type = str, default = 'rawtoall_data18_mt8',
                        help = 'Name of the job for which to create the plots')
    parser.add_argument('-r', '--release', type = str, default = 'master',
                        help = 'Name of the release for which to create the plots ')
    parser.add_argument('-p', '--platform', type = str, default = 'x86_64-centos7-gcc8-opt',
                        help = 'Name of the platform for which to create the plots ')
    parser.add_argument('-s', '--steps', type = str, default = 'RAWtoALL',
                        help = 'Name of the step(s) for which to create the plots (comma separated list)')
    parser.add_argument('-w', '--html', action = 'store_true',
                        help = 'Create HTML snippets for the plots')
    parser.add_argument('-d', '--days', type = int, default = 14,
                        help = 'Number of days to show on the plots')
    parser.add_argument('-c', '--components', type = int, default = 10,
                        help = 'Number of components/containers to show on the plots')

    args = parser.parse_args()

    inputdb      = args.input
    outdir       = args.outdir
    jobname      = args.jobname
    jobrelease   = args.release
    jobplatform  = args.platform
    jobsteps     = [ val for val in args.steps.split(',') ]
    createhtml   = args.html
    days_to_keep = args.days
    plot_top_n   = args.components

    # Print some information
    print('='*100)
    print(f"Creating plots {'and html snippets' if createhtml else ''} for {jobname}, {jobrelease}, {jobplatform}, {jobsteps}")
    print(f"Top {plot_top_n} components/containers for the the last {days_to_keep} days will be plotted")
    print(f"Input database file is : {inputdb}")
    print(f"Output directory is    : {outdir}")
    print('='*100)

    # Create the DB connetcion
    if not path.exists(inputdb):
        print(f"Input file {inputdb} doesn't exists, nothing to do...")
        return
    db = sa.create_engine(f"sqlite:///{inputdb}")
    con = db.connect()
    from sqlalchemy import text
    # Loop over all steps that are requested...
    for jobstep in jobsteps:

        # Read all the tables - can do this only once but that's OK for now
        jobs = pd.read_sql_query(text('SELECT * from job'), con)
        snaps = pd.read_sql_query(text('SELECT * from snapshot'), con)
        comps = pd.read_sql_query(text('SELECT * from component'), con)
        conts = pd.read_sql_query(text('SELECT * from container'), con)

        # Only keep the succesful jobs in the last N days for the job/release/platform/step that's asked for
        jobs = jobs[jobs.name.str.fullmatch(jobname) &
                    jobs.release.str.fullmatch(jobrelease) &
                    jobs.platform.str.fullmatch(jobplatform) &
                    jobs.step.str.fullmatch(jobstep) &
                    (jobs.exitcode == 0)]
        jobs = jobs.sort_values(by = 'date', ascending = False).head(days_to_keep)
        plot_order = jobs.sort_values(by = 'date')['nightly'] # the order for the x-axis
        nightlies = list(plot_order)[::-1] # The list of nightlies

        # Now prepare the joined tables only keeping some relevant columns and sort by date
        snaps = snaps.merge(jobs[['jid','name','release','platform','step','nightly','nevents','date']], on = 'jid', how = 'inner')
        snaps = snaps.sort_values(by = 'date', ascending = False)

        comps = comps.merge(jobs[['jid','name','release','platform','step','nightly','nevents','date']], on = 'jid', how = 'inner')
        comps = comps.rename(columns = {'name_x' : 'component', 'name_y' : 'job'})
        comps = comps.sort_values(by = 'date', ascending = False)
        # Shorten long component names, i.e. BEGIN[...]END
        comp_keep_chars = 20 # chars to keep at the beginning and end
        comps['component'] = comps['component'].apply(lambda x: f"{x[:comp_keep_chars]}[...]{x[-comp_keep_chars:]}" if len(x) > 2*comp_keep_chars+5 and 'FldrCache' not in x else x)

        conts = conts.merge(jobs[['jid','name','release','platform','step','nightly','nevents','date']], on = 'jid', how = 'inner')
        conts = conts.rename(columns = {'name_x' : 'container', 'name_y' : 'job'})
        conts = conts.sort_values(by = 'date', ascending = False)
        # Shorten long container names, i.e. BEGIN[...]END
        cont_keep_chars = 20 # chars to keep at the beginning and end
        conts['container'] = conts['container'].apply(lambda x: f"{x[:cont_keep_chars]}[...]{x[-cont_keep_chars:]}" if len(x) > 2*cont_keep_chars+5 else x)

        print(f"Creating plots for step {jobstep}...")

        ##########################
        ## Job-level Plots
        ##########################

        # Create some useful metrics on-the-fly
        jobs['pss_per_thread'] = jobs['max_pss']/jobs['nthreads']/1024. # Convert to MB
        jobs['vmem_per_thread'] = jobs['max_vmem']/jobs['nthreads']/1024. # Convert to MB
        jobs['cpu_per_event'] = jobs['cpu_per_event']*0.001 # Convert to seconds
        jobs['wall_per_event'] = jobs['wall_per_event']*0.001 # Convert to seconds
        jobs['throughput'] = 1./jobs['wall_per_event']

        # Create the summary plots
        create_summary_plots(jobs, f"{outdir}/summary/{jobrelease}__{jobplatform}", f"Nightly\nRelease: {jobrelease}\nPlatform: {jobplatform}\nStep: {jobstep}", f"{jobname}", f"{jobstep}")

        ##########################
        ## Snapshot-level Plots
        ##########################

        # Create some useful metrics on-the-fly
        snaps['delta_vmem'] = snaps['delta_vmem']/1024. # Convert to MB
        snaps['delta_pss'] = snaps['delta_pss']/1024. # Convert to MB
        snaps['delta_rss'] = snaps['delta_rss']/1024. # Convert to MB
        snaps['delta_swap'] = snaps['delta_swap']/1024. # Convert to MB

        # VMEM increase per stage
        create_pointplot(data = snaps, x = 'nightly', y = 'delta_vmem', hue = 'stage',
                         xlabel = f"Nightly\nRelease: {jobrelease}\nPlatform: {jobplatform}\nStep: {jobstep}", ylabel = 'VMEM [MB]', palette = 'colorblind',
                         title = f"VMEM Increase per Stage\nJob: {jobname}", order = plot_order,
                         folder = f"{outdir}/summary/{jobrelease}__{jobplatform}", fname = f"{jobstep}-delta-vmem-per-stage.png")

        # RSS increase per stage
        create_pointplot(data = snaps, x = 'nightly', y = 'delta_rss', hue = 'stage',
                         xlabel = f"Nightly\nRelease: {jobrelease}\nPlatform: {jobplatform}\nStep: {jobstep}", ylabel = 'RSS [MB]', palette = 'colorblind',
                         title = f"RSS Increase per Stage\nJob: {jobname}", order = plot_order,
                         folder = f"{outdir}/summary/{jobrelease}__{jobplatform}", fname = f"{jobstep}-delta-rss-per-stage.png")

        # PSS increase per stage
        create_pointplot(data = snaps, x = 'nightly', y = 'delta_pss', hue = 'stage',
                         xlabel = f"Nightly\nRelease: {jobrelease}\nPlatform: {jobplatform}\nStep: {jobstep}", ylabel = 'PSS [MB]', palette = 'colorblind',
                         title = f"PSS Increase per Stage\nJob: {jobname}", order = plot_order,
                         folder = f"{outdir}/summary/{jobrelease}__{jobplatform}", fname = f"{jobstep}-delta-pss-per-stage.png")

        ##########################
        ## Component-level Plots
        ##########################

        # Create some useful metrics on-the-fly
        comps['cpu_per_event'] = comps['total_cpu']/(comps['nevents'] - 1) # subtract off 1 to account for the first event here!
        comps['cpu_per_call'] = comps['total_cpu']/comps['count']
        comps['total_malloc'] = comps['total_malloc']/1024. # Convert to MB
        comps['malloc_per_event'] = comps['total_malloc']/(comps['nevents'] - 1) # subtract off 1 to account for the first event here!
        comps['malloc_per_call'] = comps['total_malloc']/comps['count']
        comps['total_vmem'] = comps['total_vmem']/1024. # Convert to MB
        comps['vmem_per_event'] = comps['total_vmem']/(comps['nevents'] - 1) # subtract off 1 to account for the first event here!
        comps['vmem_per_call'] = comps['total_vmem']/comps['count']

        # Plot the CPU-time/event grouped by domain
        df = comps[comps['stage']=='Execute'][['component','nightly','domain','cpu_per_event']]
        df = df.groupby(['nightly','domain']).agg({'cpu_per_event' : 'sum'}).reset_index()
        df = df.sort_values(by = 'cpu_per_event', ascending = False).groupby('nightly').head(99)
        create_pointplot(data = df, x = 'nightly', y = 'cpu_per_event', hue = 'domain',
                         xlabel = f"Nightly\nRelease: {jobrelease}\nPlatform: {jobplatform}\nStep: {jobstep}", ylabel = 'CPU-time/Event [ms]', palette = 'colorblind',
                         title = f"CPU-time/Event per Domain\nJob: {jobname}", order = plot_order,
                         folder = f"{outdir}/cpu/{jobrelease}__{jobplatform}", fname = f"{jobstep}-cpu-per-evt-domains.png")

        # Plot the CPU-time/event per domain
        domains = set(comps[comps['stage']=='Execute']['domain'])
        for domain in domains:
            df = comps[comps['stage']=='Execute'][['component','nightly','domain','cpu_per_event']]
            df = df[df['domain']==domain]
            df = df.sort_values(by = 'cpu_per_event', ascending = False).groupby('nightly').head(plot_top_n)
            create_pointplot(data = df, x = 'nightly', y = 'cpu_per_event', hue = 'component',
                             xlabel = f"Nightly\nRelease: {jobrelease}\nPlatform: {jobplatform}\nStep: {jobstep}", ylabel = 'CPU-time/Event [ms]', palette = 'colorblind',
                             title = f"Top {plot_top_n} CPU-time/Event ({domain})\nJob: {jobname}", order = plot_order,
                             folder = f"{outdir}/cpu/{jobrelease}__{jobplatform}", fname = f"{jobstep}-cpu-per-evt-{domain}.png")

        # Plot CPU-time/event for components vs nightly in execute
        df = comps[comps['stage']=='Execute'][['component','nightly','cpu_per_event']]
        df = df.sort_values(by = 'cpu_per_event', ascending = False).groupby('nightly').head(plot_top_n)
        create_pointplot(data = df, x = 'nightly', y = 'cpu_per_event', hue = 'component',
                         xlabel = f"Nightly\nRelease: {jobrelease}\nPlatform: {jobplatform}\nStep: {jobstep}", ylabel = 'CPU-time/Event [ms]', palette = 'colorblind',
                         title = f"Top {plot_top_n} CPU-time/Event (All Domains)\nJob: {jobname}", order = plot_order,
                         folder = f"{outdir}/cpu/{jobrelease}__{jobplatform}", fname = f"{jobstep}-cpu-per-evt-top-{plot_top_n}.png")

        # FIXME: This try-except block is a temporary workaround to allow the remaiing plots to be produced
        try:
        # Now comes the memory metrics
            for memtype in ["malloc"]: #["malloc", "vmem"]:

                # Plot total malloc(vmem) for components vs nightly in initialize
                df = comps[comps['stage']=='Initialize'][['component','nightly',f"total_{memtype}"]]
                df = df.sort_values(by = f"total_{memtype}", ascending = False).groupby('nightly').head(plot_top_n)
                create_pointplot(data = df, x = 'nightly', y = f"total_{memtype}", hue = 'component',
                                 xlabel = f"Nightly\nRelease: {jobrelease}\nPlatform: {jobplatform}\nStep: {jobstep}", ylabel = 'Memory Allocations [MB]', palette = 'colorblind',
                                 title = f"Top {plot_top_n} Memory Allocations (Initialize)\nJob: {jobname}", order = plot_order,
                                 folder = f"{outdir}/{memtype}/{jobrelease}__{jobplatform}", fname = f"{jobstep}-{memtype}-per-comp-init.png")

                # Plot malloc(vmem)/event and malloc(vmem)/call for components vs nightly in execute
                # This bit is enabled only for single threaded jobs...
                if jobs['nthreads'].iloc[0] == 1:

                    df = comps[comps['stage']=='FirstEvent'][['component','nightly',f"total_{memtype}"]]
                    df = df.sort_values(by = f"total_{memtype}", ascending = False).groupby('nightly').head(plot_top_n)
                    create_pointplot(data = df, x = 'nightly', y = f"total_{memtype}", hue = 'component',
                                     xlabel = f"Nightly\nRelease: {jobrelease}\nPlatform: {jobplatform}\nStep: {jobstep}", ylabel = 'Memory Allocations [MB]', palette = 'colorblind',
                                     title = f"Top {plot_top_n} Memory Allocations (First Event)\nJob: {jobname}", order = plot_order,
                                     folder = f"{outdir}/{memtype}/{jobrelease}__{jobplatform}", fname = f"{jobstep}-{memtype}-per-comp-firstevt.png")

                    df = comps[comps['stage']=='Execute'][['component','nightly',f"{memtype}_per_event"]]
                    df = df.sort_values(by = f"{memtype}_per_event", ascending = False).groupby('nightly').head(plot_top_n)
                    create_pointplot(data = df, x = 'nightly', y = f"{memtype}_per_event", hue = 'component',
                                     xlabel = f"Nightly\nRelease: {jobrelease}\nPlatform: {jobplatform}\nStep: {jobstep}", ylabel = 'Memory Allocations/Event [MB]', palette = 'colorblind',
                                     title = f"Top {plot_top_n} Memory Allocations/Event (Execute)\nJob: {jobname}", order = plot_order,
                                     folder = f"{outdir}/{memtype}/{jobrelease}__{jobplatform}", fname = f"{jobstep}-{memtype}-per-evt-top-{plot_top_n}.png")

                    df = comps[comps['stage']=='Execute'][['component','nightly',f"{memtype}_per_call"]]
                    df = df.sort_values(by = f"{memtype}_per_call", ascending = False).groupby('nightly').head(plot_top_n)
                    create_pointplot(data = df, x = 'nightly', y = f"{memtype}_per_call", hue = 'component',
                                     xlabel = f"Nightly\nRelease: {jobrelease}\nPlatform: {jobplatform}\nStep: {jobstep}", ylabel = 'Memory Allocations/Call [MB]', palette = 'colorblind',
                                     title = f"Top {plot_top_n} Memory Allocations/Call (Execute)\nJob: {jobname}", order = plot_order,
                                     folder = f"{outdir}/{memtype}/{jobrelease}__{jobplatform}", fname = f"{jobstep}-{memtype}-per-call-top-{plot_top_n}.png")

                    df = comps[comps['stage']=='preLoadProxy'][['component','nightly',f"{memtype}_per_call"]]
                    df = df[df['component'].str.contains('loadCachesOverhead:')] # Keep top-level loadCachesOverhead calls
                    df['component'] = df['component'].str.replace('loadCachesOverhead:','') # Strip loadCachesOverhead from the name
                    df = df.sort_values(by = f"{memtype}_per_call", ascending = False).groupby('nightly').head(plot_top_n)
                    create_pointplot(data = df, x = 'nightly', y = f"{memtype}_per_call", hue = 'component',
                                     xlabel = f"Nightly\nRelease: {jobrelease}\nPlatform: {jobplatform}\nStep: {jobstep}", ylabel = 'Memory Allocations [MB]', palette = 'colorblind',
                                     title = f"Top {plot_top_n} Memory Allocations/Call (DB Schema)\nJob: {jobname}", order = plot_order,
                                     folder = f"{outdir}/{memtype}/{jobrelease}__{jobplatform}", fname = f"{jobstep}-{memtype}-per-comp-preloadproxy-schema.png")

                    df = comps[comps['stage']=='preLoadProxy'][['component','nightly',f"{memtype}_per_call"]]
                    df = df[df['component'].str.contains('FldrCache:')] # Keep low-level FldrCache calls
                    df['component'] = df['component'].str.replace('FldrCache:','') # Strip FldrCache from the name
                    df = df.sort_values(by = f"{memtype}_per_call", ascending = False).groupby('nightly').head(plot_top_n)
                    create_pointplot(data = df, x = 'nightly', y = f"{memtype}_per_call", hue = 'component',
                                     xlabel = f"Nightly\nRelease: {jobrelease}\nPlatform: {jobplatform}\nStep: {jobstep}", ylabel = 'Memory Allocations [MB]', palette = 'colorblind',
                                     title = f"Top {plot_top_n} Memory Allocations/Call (DB Folder)\nJob: {jobname}", order = plot_order,
                                     folder = f"{outdir}/{memtype}/{jobrelease}__{jobplatform}", fname = f"{jobstep}-{memtype}-per-comp-preloadproxy-foldercache.png")

                # Plot total malloc(vmem) for components vs nightly in initialize
                df = comps[comps['stage']=='Finalize'][['component','nightly',f"total_{memtype}"]]
                df = df.sort_values(by = f"total_{memtype}", ascending = True).groupby('nightly').head(plot_top_n)
                create_pointplot(data = df, x = 'nightly', y = f"total_{memtype}", hue = 'component',
                                 xlabel = f"Nightly\nRelease: {jobrelease}\nPlatform: {jobplatform}\nStep: {jobstep}", ylabel = 'Memory Allocations [MB]', palette = 'colorblind',
                                 title = f"Top {plot_top_n} Memory Allocations (Finalize)\nJob: {jobname}", order = plot_order,
                                 folder = f"{outdir}/{memtype}/{jobrelease}__{jobplatform}", fname = f"{jobstep}-{memtype}-per-comp-fin.png")
        except IndexError as e:
            print(e)


        ##########################
        ## Container-level Plots
        ##########################

        if not conts.empty:
            # Plot the container sizes grouped by domain
            df = conts[conts['datatype'] == 'EventData']
            df = df.groupby(['nightly','domain']).agg({'sizeperevt' : 'sum'}).reset_index()
            df = df.sort_values(by = 'sizeperevt', ascending = False).groupby('nightly').head(99)
            create_pointplot(data = df, x = 'nightly', y = 'sizeperevt', hue = 'domain',
                             xlabel = f"Nightly\nRelease: {jobrelease}\nPlatform: {jobplatform}\nStep: {jobstep}", ylabel = 'Size/Event [kb]', palette = 'colorblind',
                             title = f"Container Sizes per Domain\nJob: {jobname}", order = plot_order,
                             folder = f"{outdir}/sizeperevt/{jobrelease}__{jobplatform}", fname = f"{jobstep}-size-per-evt-domains.png")

            # Plot the container sizes grouped by domain - pie chart
            df_pie = df[df['nightly'] == nightlies[0]]
            df_pie = df_pie.sort_values(by = 'sizeperevt', ascending = False).reset_index()[['domain','sizeperevt']]
            threshold_pie = 0.05 * sum(df_pie['sizeperevt']) # 5% of the total event size
            others_total_pie = sum(df_pie[df_pie['sizeperevt'] < threshold_pie]['sizeperevt'])
            df_pie = df_pie[df_pie['sizeperevt'] >= threshold_pie]
            df_pie.loc[len(df_pie.index)] = ['Other', others_total_pie]
            fig_pie, ax_pie = plt.subplots()
            patches, texts, pcts = ax_pie.pie(df_pie['sizeperevt'],
                                              labels = df_pie['domain'],
                                              startangle = 90,
                                              autopct = lambda pct: f"{pct*sum(df_pie['sizeperevt'])*0.01:.2f} KB\n({pct:.2f}%)",
                                              pctdistance = 0.75,
                                              labeldistance = 1.05)
            ax_pie.axis('equal')
            plt.setp(pcts, color='white', fontweight='bold')
            ax_pie.set_title(f"Container Sizes per Domain\nJob: {jobname}")
            ax_pie.set(xlabel = f"Nightly: {nightlies[0]}\nRelease: {jobrelease} - Platform: {jobplatform} - Step: {jobstep}\nTotal Event Size: {sum(df_pie['sizeperevt']):.2f} KB")
            plt.savefig(f"{outdir}/sizeperevt/{jobrelease}__{jobplatform}/{jobstep}-size-per-evt-domainsi-pie.png")
            plt.close()

            # Plot the container sizes per domain
            domains = set(conts[conts['datatype'] == 'EventData']['domain'])
            for domain in domains:
                df = conts[conts['domain']==domain]
                df = df.sort_values(by = 'sizeperevt', ascending = False).groupby('nightly').head(plot_top_n)
                create_pointplot(data = df, x = 'nightly', y = 'sizeperevt', hue = 'container',
                                 xlabel = f"Nightly\nRelease: {jobrelease}\nPlatform: {jobplatform}\nStep: {jobstep}", ylabel = 'Size/Event [kb]', palette = 'colorblind',
                                 title = f"Top {plot_top_n} Container Sizes ({domain})\nJob: {jobname}\nStep: {jobstep}", order = plot_order,
                                 folder = f"{outdir}/sizeperevt/{jobrelease}__{jobplatform}", fname = f"{jobstep}-size-per-evt-{domain}.png")

            # Plot the container sizes per domain
            df = conts[conts['datatype'] == 'EventData'][['container','nightly','sizeperevt']]
            df = df.sort_values(by = 'sizeperevt', ascending = False).groupby('nightly').head(plot_top_n)
            create_pointplot(data = df, x = 'nightly', y = 'sizeperevt', hue = 'container',
                             xlabel = f"Nightly\nRelease: {jobrelease}\nPlatform: {jobplatform}\nStep: {jobstep}", ylabel = 'Size/Event [kb]', palette = 'colorblind',
                             title = f"Top {plot_top_n} Container Sizes (All Domains)\nJob: {jobname}\nStep: {jobstep}", order = plot_order,
                             folder = f"{outdir}/sizeperevt/{jobrelease}__{jobplatform}", fname = f"{jobstep}-size-per-evt-{plot_top_n}.png")
        else:
            print(f">> Container-level plots will not be available for step {jobstep}...")

    # Create the html snippet if the user asked for it
    if createhtml:
        print('Creating HTML snippets...')
        create_html_snippet(outdir, (jobname, jobplatform, jobrelease, jobsteps, nightlies))

    # Close connection
    con.close()

    # Print and quit
    print('All done, goodbye...')
    print('='*100)

if '__main__' in __name__:
    main()
