#!/usr/bin/env python3

__author__ = "Alaettin Serhan Mete <amete@cern.ch>"
__version__ = "0.0.1"

import SPOTDatabase as sdb
import argparse
import glob
import os

def main():
    """ Main function """

    # Parse the user inputs
    parser = argparse.ArgumentParser(description = 'Script to merge database files for the daily SPOT monitoring job')

    parser.add_argument('-i', '--inputs', type = str, required = True,
                        help = 'Input database files to be merged')
    parser.add_argument('-o', '--output', type = str, required = True,
                        help = 'Output database file')

    args = parser.parse_args()

    inputs = args.inputs
    output = args.output

    file_list = glob.glob(inputs)
    
    if os.path.exists(output):
        print(f"Merging results of {file_list} into {output}")
        myobj = sdb.SPOTDatabase(output)
        myobj.mergeDBFiles(file_list)
    else:
        print(f"Copying and merging results of {file_list} into {output}")
        os.system(f"cp {file_list[0]} {output}")
        if len(file_list) > 1:
            myobj = sdb.SPOTDatabase(output)
            myobj.mergeDBFiles(file_list[1:])

if '__main__' in __name__:
    main()
