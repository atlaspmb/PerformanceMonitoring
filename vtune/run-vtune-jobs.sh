#!/bin/bash

# RAWtoALL + DQ
run_r2a_data23() {
    # See which analysis we are running...

    # Parse the inputs
    NTHREADS=${1}
    NEVENTS=${2}

    # Run the job
    export TRF_ECHO=1;
    LD_PRELOAD="${TCMALLOCDIR}/libtcmalloc_minimal.so:${ATLASMKLLIBDIR_PRELOAD}/libimf.so" \
    ATHENA_CORE_NUMBER=${NTHREADS} \
    Reco_tf.py \
      --CA  'True' \
      --perfmon 'none' \
      --inputBSFile '/eos/atlas/atlascerngroupdisk/proj-spot/spot-job-inputs/data23_13p6TeV/data23_13p6TeV.00451569.physics_Main.daq.RAW._lb0260._SFO-14._0001.data' \
      --maxEvents ${NEVENTS} \
      --outputAODFile 'myAOD.pool.root' \
      --outputHISTFile 'myHIST.root' \
      --multithreaded 'True' \
      --autoConfiguration 'everything' \
      --conditionsTag 'CONDBR2-BLKPA-2023-01' \
      --geometryVersion 'ATLAS-R3S-2021-03-02-00' \
      --runNumber '451569' \
      --vtune "True" \
      --vtuneExtraOpts="-collect=hotspots" \
      --steering 'doRAWtoALL' > __log.txt 2>&1;

    # Get the exit code
    echo $? > __exitcode;

}

# ttbar full-chain MC
run_mcttbar(){
    
    # Parse the inputs
    NTHREADS=${1}
    NEVENTS=${2}
    
    # Run the job
    export TRF_ECHO=1;
    LD_PRELOAD="${TCMALLOCDIR}/libtcmalloc_minimal.so:${ATLASMKLLIBDIR_PRELOAD}/libimf.so" \
    ATHENA_CORE_NUMBER=${NTHREADS} Sim_tf.py \
      --perfmon 'none' \
      --CA 'True'\
      --multithreaded 'True' \
      --inputEVNTFile '/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/CampaignInputs/mc23/EVNT/mc23_13p6TeV.601229.PhPy8EG_A14_ttbar_hdamp258p75_SingleLep.evgen.EVNT.e8514/EVNT.32288062._002040.pool.root.1' \
      --conditionsTag 'default:OFLCOND-MC21-SDR-RUN3-07' \
      --geometryVersion 'default:ATLAS-R3S-2021-03-02-00'   \
      --AMIConfig 's4006' \
      --outputHITSFile 'myHITS.pool.root' \
      --maxEvents ${NEVENTS} \
      --vtune "True" \
      --vtuneExtraOpts="-collect=hotspots" \
      --jobNumber '1'  > __log.txt 2>&1;

    # Get the exit code
    echo $? > __exitcode;

}

# Define and execute the test
execute() {

    # Define test parameters
    JOBNAME="${1}";
    JOBRELEASE="${2}";
    JOBPLATFORM="${3}";

    echo "${JOBNAME} - ${JOBRELEASE} - ${JOBPLATFORM}"

    # Define the top-level workdir
    WORKDIR="/data/atlaspmb/athenamt-perfmonmt-jobs/vtune/hotspot";

    # Create the rundir
    RUNDIR="${WORKDIR}/${JOBNAME}/${JOBRELEASE}/${JOBPLATFORM}";
    if [[ ! -d ${RUNDIR} ]]; then
        mkdir -p ${RUNDIR};
    fi

    # Go to the main rundir
    echo "Using ${RUNDIR} as the rundir...";
    cd "${RUNDIR}";
    export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
    source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
    ulimit -n 4096
    source /cvmfs/projects.cern.ch/intelsw/oneAPI/linux/all-setup.sh;


    # Setup the latest Athena - job runs once per day at a fixed time
    lsetup "asetup Athena,${JOBRELEASE},${JOBPLATFORM//-/,},latest";

    # Check the currently nightly tag
    nightly=`echo "${Athena_DIR##*/${JOBRELEASE}_Athena_${JOBPLATFORM}/}"`;
    nightly=`echo "${nightly%%/Athena/*}"`;

    # Check if it exists already
    if [[ -d "${nightly}" ]]; then
        echo "Directory for ${nightly} already exists, nothing to do."
        return 0;
    fi

    # Now setup the run directory
    mkdir -p "${nightly}"; 
    cd "${nightly}";

    # Let's start
    touch __start;


    if [[ "${JOBNAME}" == "rawtoall_data23_mt8" ]]; then
        run_r2a_data23 8 1000; # 1000 events in 8 threads
    elif [[ "${JOBNAME}" == "ttbar_fullsim_mt8" ]]; then
         run_mcttbar 8 100; # 100 events in 8 threads
    else
        echo "Unknown job ${JOBNAME}, quitting..."
        return 0
    fi


    # Let's extract the transform command to be used on the webpage
    echo "#!/bin/bash" > __command.txt;
    if [[ -f "env.txt" ]]; then
        echo "export $( grep "ATHENA_CORE_NUMBER" env.txt )" >> __command.txt;
    fi

    # All done
    touch __done;

 #   rsync -avuz "${RUNDIR}/${nightly}" aiatlasbm001.cern.ch:"/${RUNDIR}"; 
    # Go back to rundir
    cd "${RUNDIR}";

}

# Define the main function
main() {
    # Setup environment
    source ~/.bashrc;
    source ~/.bash_profile;

    pidof -o %PPID -x $0 >/dev/null && echo "WARNING: Script ${0} already running, nothing to do..." && exit 0

    # These are the standard jobs
    JOBS=( "main x86_64-el9-gcc13-opt rawtoall_data23_mt8" /
               "main x86_64-el9-gcc13-opt ttbar_fullsim_mt8" )

    # Loop over all defined jobs
    for job in "${JOBS[@]}"
    do
        # Parse the inputs: release platform name
        vals=(${job})
        jobrelease="${vals[0]}"
        jobplatform="${vals[1]}"
        jobname="${vals[2]}"
        # Match the release w/ the user input
        execute "${jobname}" "${jobrelease}" "${jobplatform}"
    done

}

# Execute the main function
main
