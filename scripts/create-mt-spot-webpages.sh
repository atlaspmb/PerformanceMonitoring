#!/bin/bash

# Define and execute the main job
execute() {

    # Define test parameters
    JOBNAME="${1}";
    JOBRELEASE="${2}";
    JOBPLATFORM="${3}";

    # Figure out the relevant steps, can be coma separated e.g. StepA,StepB etc.
    declare -A JOBSTEPS=( ["rawtoall_data18_mt16"]="RAWtoALL"
                          ["rawtoall_data18_mt8"]="RAWtoALL"
                          ["rawtoall_data18_mt1"]="RAWtoALL"
                          ["rawtoall_data22_mt8"]="RAWtoALL"
                          ["rawtoall_data23_mt1"]="RAWtoALL"
                          ["rawtoall_data23_mt8"]="RAWtoALL"
                          ["rawtoall_r3_data23_mt8"]="RAWtoALL"
                          ["rawtoall_r3_data23_mt1"]="RAWtoALL"
                          ["rawtoall_data22_mt1"]="RAWtoALL"
                          ["evnttohits_itk_ca_mt16"]="EVNTtoHITS"
                          ["rdotordotrigger_q221_mt8"]="RDOtoRDOTrigger"
                          ["fullchain_q445_mt8"]="HITtoRDO,RDOtoRDOTrigger,RAWtoALL"
                          ["fullchain_mc21a_mt8"]="Overlay,RDOtoRDOTrigger,RAWtoALL"
                          ["fullchain_mc23c_mt8"]="Overlay,RDOtoRDOTrigger,RAWtoALL"
                          ["fullchain_mc23c_mt1"]="Overlay,RDOtoRDOTrigger,RAWtoALL"
                          ["ttbar_fullsim_mt8"]="EVNTtoHITS"
                          ["ttbar_fastsim_mt8"]="EVNTtoHITS"
                          ["ttbar_digi_serial"]="HITtoRDO"
                          ["digi_serial_mc23c"]="HITtoRDO"
                          ["digi_serial_phase2"]="HITtoRDO"
                          ["phase2_fullsim_mt8"]="EVNTtoHITS"
                          ["phase2_fullsim"]="EVNTtoHITS"
                          ["phase2_recoonly_fasttracking"]="RAWtoALL"
                          ["phase2_recoonly_actsfasttracking"]="RAWtoALL"
                          ["phase2_recoonly_tracking"]="RAWtoALL"
                          ["phase2_recoonly_actstracking"]="RAWtoALL"
                          ["phase2_recoonly_actstracking_cached"]="RAWtoALL"
                          ["phase2_recoonly_actsfasttracking_HI"]="RAWtoALL"
                          ["data22_daod_phys"]="Derivation"
                          ["data23_daod_phys"]="Derivation"
                          ["data23_daod_phys_ntp"]="Derivation"
                          ["mc21a_daod_phys"]="Derivation"
                          ["mc21a_daod_phys_ntp"]="Derivation"
                          ["data22_daod_physlite"]="Derivation"
                          ["mc21a_daod_physlite"]="Derivation" 
                          ["mc21a_daod_physlite_ntp"]="Derivation" 
                          ["data23_daod_physlite"]="Derivation"
                          ["data23_daod_physlite_ntp"]="Derivation"
                          ["hp_recoonly_mt8"]="RAWtoALL"
                          ["upc_recoonly_mt8"]="RAWtoALL"
		          ["cp_tools_analysis"]="CPAnalysis")

    # Define the top-level workdir
    WORKDIR="/data/atlaspmb/athenamt-perfmonmt-jobs";

    # Create the rundir
    RUNDIR="${WORKDIR}/${JOBNAME}/${JOBRELEASE}/${JOBPLATFORM}";
    if [[ ! -d ${RUNDIR} ]]; then
        echo "Directory ${RUNDIR} doesn't exist, nothing to do...";
        return 0;
    fi

    # Setup the environment
    BASEDIR="/afs/cern.ch/atlas/project/pmb/new_pmb/PerformanceMonitoring";
    lsetup "views LCG_104d ${JOBPLATFORM}"
    export PYTHONPATH=$BASEDIR/python:$PYTHONPATH;

    # Go to the workdir
    cd ${WORKDIR};

    # Create the database files
    python3 $BASEDIR/scripts/create_spot_db.py -i ${RUNDIR} -o ${WORKDIR}/dbfiles \
        -j ${JOBNAME} -r ${JOBRELEASE} -p ${JOBPLATFORM};

    # Merge the database files
    if [[ -f dbfiles/${JOBNAME}.db ]]; then
      echo "Removing existing merged database file...";
      rm -f dbfiles/${JOBNAME}.db;
    fi
    python3 $BASEDIR/scripts/merge_spot_db.py -i "${WORKDIR}/dbfiles/*__${JOBNAME}__*.db" -o ${WORKDIR}/dbfiles/${JOBNAME}.db;

    # Create the webpage plots and html files
    python3 $BASEDIR/scripts/create_spot_plots.py -i ${WORKDIR}/dbfiles/${JOBNAME}.db -o ${HOME}/www_atlaspmb/spot-mon-${JOBNAME}/pages \
        -j ${JOBNAME} -r ${JOBRELEASE} -p ${JOBPLATFORM} -s ${JOBSTEPS[${JOBNAME}]} -w;

}

# Define the main function
main() {

    # Setup environment
    source ~/.bashrc;
    source ~/.bash_profile;
    eosfusebind -g # ATLINFR-3256
    if [[ "${1}" == "el9_test" ]]; then
        # These are jobs to test the new EL9 OS
        JOBS=("24.0 x86_64-el9-gcc13-opt rawtoall_data23_mt8" \
               "24.0 x86_64-el9-gcc13-opt fullchain_q445_mt8" \
               "24.0 x86_64-el9-gcc13-opt rawtoall_data23_mt1" \
               "24.0 x86_64-el9-gcc13-opt fullchain_mc23c_mt8" \
               "24.0 x86_64-el9-gcc13-opt rawtoall_r3_data23_mt8" \
               "24.0 x86_64-el9-gcc13-opt rawtoall_r3_data23_mt1" \
               "24.0 x86_64-el9-gcc13-opt digi_serial_mc23c" \
               "24.0 x86_64-el9-gcc13-opt ttbar_fullsim_mt8" \
               "24.0 x86_64-el9-gcc13-opt ttbar_fastsim_mt8" \
               "24.0 x86_64-el9-gcc13-opt hp_recoonly_mt8" \
               "24.0 x86_64-el9-gcc13-opt upc_recoonly_mt8" \
               "main x86_64-el9-gcc13-opt rawtoall_data23_mt8" \
               "main x86_64-el9-gcc13-opt rawtoall_r3_data23_mt8" \
               "main x86_64-el9-gcc13-opt fullchain_mc23c_mt8" \
               "main x86_64-el9-gcc13-opt fullchain_mc23c_mt1" \
               "main x86_64-el9-gcc13-opt fullchain_q445_mt8" \
               "main x86_64-el9-gcc13-opt rawtoall_data23_mt1" \
               "main x86_64-el9-gcc13-opt rawtoall_r3_data23_mt1" \
               "main x86_64-el9-gcc13-opt ttbar_fullsim_mt8" \
               "main x86_64-el9-gcc13-opt ttbar_fastsim_mt8"  \
               "main x86_64-el9-gcc13-opt digi_serial_mc23c" \
               "main x86_64-el9-gcc13-opt hp_recoonly_mt8" \
               "main x86_64-el9-gcc13-opt upc_recoonly_mt8" \
               "main x86_64-el9-gcc13-opt data22_daod_phys" \
               "main x86_64-el9-gcc13-opt mc21a_daod_phys" \
               "main x86_64-el9-gcc13-opt data23_daod_phys" \
               "main x86_64-el9-gcc13-opt data22_daod_physlite" \
               "main x86_64-el9-gcc13-opt data23_daod_phys_ntp" \
               "main x86_64-el9-gcc13-opt data23_daod_physlite_ntp" \
               "main x86_64-el9-gcc13-opt mc21a_daod_physlite_ntp" \
               "main x86_64-el9-gcc13-opt mc21a_daod_phys_ntp" \
               "main x86_64-el9-gcc13-opt mc21a_daod_physlite" \
               "main x86_64-el9-gcc13-opt data23_daod_physlite" \
               "main x86_64-el9-gcc13-opt phase2_fullsim_mt8" \
               "main x86_64-el9-gcc13-opt phase2_recoonly_fasttracking" \
               "main x86_64-el9-gcc13-opt phase2_recoonly_tracking" \
               "main x86_64-el9-gcc13-opt phase2_recoonly_actstracking" \
               "main x86_64-el9-gcc13-opt phase2_recoonly_actsfasttracking" \
               "main x86_64-el9-gcc13-opt phase2_recoonly_actstracking_cached" \
               "main x86_64-el9-gcc13-opt phase2_recoonly_actsfasttracking_HI" \
               "main--ACTS x86_64-el9-gcc13-opt phase2_recoonly_actstracking" \
               "main--ACTS x86_64-el9-gcc13-opt phase2_recoonly_actsfasttracking" \
               "main x86_64-el9-gcc13-opt digi_serial_phase2" \
               "main--HepMC2 x86_64-el9-gcc13-opt fullchain_mc23c_mt8" \
               "main--HepMC2 x86_64-el9-gcc13-opt digi_serial_mc23c" \
               "main x86_64-el9-gcc13-opt cp_tools_analysis" )
    else
        JOBS=()
    fi

    # Loop over all defined jobs
    for job in "${JOBS[@]}"
    do
        # Parse the inputs: release platform name
        vals=(${job})
        jobrelease="${vals[0]}"
        jobplatform="${vals[1]}"
        jobname="${vals[2]}"
        # Process the job
        execute "${jobname}" "${jobrelease}" "${jobplatform}"
    done

}

# Execute main
main "${1}" 
