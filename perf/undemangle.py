import subprocess
import argparse
def demangle(symbol):
    """Demangle a single C++ symbol using c++filt."""
    try:
        return subprocess.check_output(["c++filt", symbol], text=True).strip()
    except:
        return symbol  # Return original if demangling fails
def main():
  parser = argparse.ArgumentParser(description = 'Script to create plots for the daily SPOT monitoring webpages')
  parser.add_argument('-i', '--inputfile', type = str, required = True,
                        help = 'Input SQL DB file')
  parser.add_argument('-o', '--outfile', type = str, required = True,
                        help = 'Output directory for the plots')
  args = parser.parse_args()
  inputf      = args.inputfile
  outf       = args.outfile
  # Process the file
  print (inputf)
  with open(inputf, "r") as infile, open(f"{outf}", "w") as outfile:
    for line in infile:
        print (line)
        parts = line.rstrip().split()  # Split by whitespace
        if parts and parts[-1].startswith("_Z"):  # Check if last column is a mangled symbol
            parts[-1] = demangle(parts[-1])  # Demangle last column only
        outfile.write(" ".join(parts) + "\n")  # Join and write back

  print("Demangling completed! Output saved to perf_hotspots_demangled.txt")
if '__main__' in __name__:
    main()


