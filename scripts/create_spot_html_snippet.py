#!/usr/bin/env python3
__author__ = "Alaettin Serhan Mete <amete@cern.ch>"
__version__ = "0.0.1"

from os import path
import argparse
import glob

def sort_plots(x):
    """ Basic function to sort plots """
    # Offset for the workflow
    offset = 999
    if 'EVNTtoHITS' in x:
        offset = 0
    elif 'HITtoRDO' in x or 'Overlay' in x:
        offset = 100
    elif 'RDOtoRDOTrigger' in x:
        offset = 200
    elif 'RAWtoESD' in x:
        offset = 300
    elif 'ESDtoAOD' in x:
        offset = 400
    elif 'RAWtoALL' in x:
        offset = 500
    elif 'AODtoDAOD' in x:
        offset = 600
    # Ordering within a workflow
    result = 9
    if 'malloc' in x:
        if 'init' in x:
            result = 0
        elif 'firstevt' in x:
            result = 1
        elif 'top-' in x:
            result = 2
        elif 'fin' in x:
            result = 3
        else:
            result = 4
    else:
        if 'pie' in x:
            result = 0
        elif 'domain' in x:
            result = 1
        elif 'top-' in x:
            result = 2
        else:
            result = 3
    result += offset
    return result

def add_info_text(folder, filehandle,release, platform):
    """ Add some basic text to the relevant page """

    if "summary" in folder:
        filehandle.write('<div style="margin:auto;text-align:center;font-size:80%;color:gray;padding:10px">\n')
        filehandle.write('  Welcome to the ATLAS SPOT performance monitoring pages where various computing performance metrics are plotted.<br>\n')
        filehandle.write('  The current page contains plots providing a high-level overview of the CPU and memory usage in the associated job.<br>\n')
        filehandle.write('  Please use the menu at the top of the page to navigate to more detailed information.<br>\n')
        filehandle.write(f"  You can use the links provided in the tables below to compare MRs between consecutive nightlies and find commands/logs for each presented job. Platform {platform}, release {release}. <br>\n")
        filehandle.write('</div>\n')
        filehandle.write('<br>\n')
    elif "cpu" in folder:
        filehandle.write('<div style="margin:auto;text-align:center;font-size:80%;color:gray;padding:10px">\n')
        filehandle.write('  The current page contains CPU utilization plots per component in the event-loop for the associated job.<br>\n')
        filehandle.write(f"  You can use the links provided in the tables below to compare MRs between consecutive nightlies and find commands/logs for each presented job. Platform {platform}, release {release}.<br>\n")
        filehandle.write('</div>\n')
        filehandle.write('<br>\n')
    elif "malloc" in folder or "vmem" in folder:
        filehandle.write('<div style="margin:auto;text-align:center;font-size:80%;color:gray;padding:10px">\n')
        filehandle.write('  The current page contains memory allocation plots per component in various steps for the associated job.<br>\n')
        filehandle.write(f"  You can use the links provided in the tables below to compare MRs between consecutive nightlies and find commands/logs for each presented job. Platform {platform}, release {release}.<br>\n")
        filehandle.write('</div>\n')
        filehandle.write('<br>\n')
    elif "sizeperevt" in folder:
        filehandle.write('<div style="margin:auto;text-align:center;font-size:80%;color:gray;padding:10px">\n')
        filehandle.write('  The current page contains event-size plots per container in the output(s) for the associated job.<br>\n')
        filehandle.write(f"  You can use the links provided in the tables below to compare MRs between consecutive nightlies and find commands/logs for each presented job. Platform {platform}, release {release}. <br>\n")
        filehandle.write('</div>\n')
        filehandle.write('<br>\n')
    else:
        filehandle.write('<br>\n')

def create_html_snippet(directory, table = None):
    """ Main function for creating html snippets """

    # If a specific release/platform is provided build the page only for it.
    # Otherwise, build the page for all subfolders.
    if table is not None:
        name, platform, release, steps, nightlies = table
        subfolders = glob.glob(f"{directory}/*/{release}__{platform}")
    else:
        subfolders = glob.glob(f"{directory}/*/*")

    # Loop over all subfolders and build an index file for each
    for folder in subfolders:

        # Skip the folders for logs and commands
        if "/logs/" in folder or "/commands/" in folder:
            continue

        # Find all the plots
        plots = glob.glob(f"{folder}/*.png")
        plots = [ plot.split('/')[-1] for plot in plots ]

        # If applicable first plot domains and then top N
        plots = sorted(plots, key = sort_plots)

        # Create a basic index file
        with open(f"{folder}/html.snippet",'w') as outfile:

            # If this is the summary page, add some basic introductory text
            add_info_text(folder, outfile, release, platform)

            # First comes the thumbnails
            outfile.write('<!-- Thumbnails -->\n')
            outfile.write('<div class="row">\n')
            outfile.write('\n')
            for idx,plot in enumerate(plots):
                outfile.write('  <div class="column">\n')
                outfile.write(f"    <img src=\"{plot}\" style=\"width:100%\" onclick=\"openModal();currentSlide({idx+1})\" class=\"hover-shadow cursor\">\n")
                outfile.write('  </div>\n')
                outfile.write('\n')
            outfile.write('</div>\n')

            # Then comes the modals
            ntotal = len(plots)
            outfile.write('\n')
            outfile.write('<!-- Modals -->\n')
            outfile.write('<div id="myModal" class="modal">\n')
            outfile.write('  <span class="close cursor" onclick="closeModal()">&times;</span>\n')
            outfile.write('  <div class="modal-content">\n')
            outfile.write('\n')
            for idx,plot in enumerate(plots):
                outfile.write('    <div class="mySlides">\n')
                outfile.write(f"      <div class=\"numbertext\">{idx+1} / {ntotal}</div>\n")
                outfile.write(f"      <img src=\"{plot}\">\n")
                outfile.write('    </div>\n')
                outfile.write('\n')
            outfile.write('\n')
            outfile.write('    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>\n')
            outfile.write('    <a class="next" onclick="plusSlides(1)">&#10095;</a>\n')
            outfile.write('\n')
            outfile.write('    <div class="caption-container">\n')
            outfile.write('      <p id="caption"></p>\n')
            outfile.write('    </div>\n')
            outfile.write('\n')
            for idx,plot in enumerate(plots):
                outfile.write('    <div class="column">\n')
                outfile.write(f"      <img class=\"demo cursor\" src=\"{plot}\" style=\"width:100%\" onclick=\"currentSlide({idx+1})\" alt=\"\">\n")
                outfile.write('    </div>\n')
                outfile.write('\n')
            outfile.write('  </div>\n')
            outfile.write('</div>\n')

            # Then comes the table w/ the nightly diffs and logs
            if table is not None:
                outfile.write('\n')
                outfile.write('<br>\n')
                outfile.write('<!-- Release Differences and Logs and Commands -->\n')
                outfile.write('<div class="row">\n')
                # Release differences
                outfile.write('  <div class="column" style="width:400px" id="diff">\n')
                outfile.write('    <table>\n')
                outfile.write('      <tr>\n')
                outfile.write('        <th>Nightly 1</th>\n')
                outfile.write('        <th>Nightly 2</th>\n')
                outfile.write('        <th>Difference</th>\n')
                outfile.write('      </tr>\n')
                for idx in range(len(nightlies)-1):
                    outfile.write('      <tr>\n')
                    outfile.write(f"        <td>{nightlies[idx+1]}</td>\n")
                    outfile.write(f"        <td>{nightlies[idx]}</td>\n")
                    link = ('https://test-atrvshft.web.cern.ch/test-atrvshft/gitlab-mr-summary-webpage/?'
                           f"merged_after={nightlies[idx+1]}&merged_before={nightlies[idx]}&target_branch={release.split('--')[0]}"
                            '&state=merged&show_details=false')
                    outfile.write(f"        <td><a href=\"{link}\">link</a></td>\n")
                    outfile.write('      </tr>\n')
                outfile.write('    </table>\n')
                outfile.write('  </div>\n')
                # Logs and Commands
                outfile.write('  <div class="column" style="width:750px" id="logs">\n')
                outfile.write('    <table>\n')
                outfile.write('      <tr>\n')
                outfile.write('        <th>Nightly</th>\n')
                outfile.write('        <th>Step</th>\n')
                outfile.write('        <th>Log</th>\n')
                outfile.write('        <th>Command</th>\n')
                outfile.write('      </tr>\n')
                for nightly in nightlies:
                    year, month, day = nightly.split('T')[0].split('-')
                    for step in steps:
                        outfile.write('      <tr>\n')
                        outfile.write(f"        <td>{nightly}</td>\n")
                        outfile.write(f"        <td>{step}</td>\n")
                        link = ('../../logs/'
                               f"{day}-{month}-{year}-{release}-{platform}-{name}-{step}")
                        outfile.write(f"        <td><a href=\"{link}\">log.{step}</a></td>\n")
                        link = ('../../commands/'
                               f"{day}-{month}-{year}-{release}-{platform}-{name}")
                        outfile.write(f"        <td><a href=\"{link}\">cmd.{step}</a></td>\n")
                        outfile.write('      </tr>\n')
                outfile.write('    </table>\n')
                outfile.write('  </div>\n')
                outfile.write('</div>\n')
                outfile.write('\n')

def main():
    """ Main function """

    # Parse the user inputs
    parser = argparse.ArgumentParser(description = 'Script to create html snippets for the daily SPOT monitoring webpages')

    parser.add_argument('-d', '--directory', type = str, required = True,
                        help = 'Directory that contains the plots')

    args = parser.parse_args()

    if not path.exists(args.directory):
        print(f"Directory {args.directory} doesn't exist, nothing to do...")
        return
    else:
        print(f"Creating html snippets using the main input/output folder {args.directory}")

    # Create the snippets
    create_html_snippet(args.directory)

if '__main__' in __name__:
    main()
