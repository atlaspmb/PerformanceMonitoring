#!/bin/bash

# RAWtoALL + DQ
run_r2a_data23() {
    # See which analysis we are running...

    # Parse the inputs
    NTHREADS=${1}
    NEVENTS=${2}
    valgrindOpts=${3}

    # Run the job
    export TRF_ECHO=1;
    Reco_tf.py \
      --CA  'True' \
      --perfmon 'none' \
      --inputBSFile '/eos/atlas/atlascerngroupdisk/proj-spot/spot-job-inputs/data23_13p6TeV/data23_13p6TeV.00451569.physics_Main.daq.RAW._lb0260._SFO-14._0001.data' \
      --maxEvents ${NEVENTS} \
      --outputAODFile 'myAOD.pool.root' \
      --outputHISTFile 'myHIST.root' \
      --autoConfiguration 'everything' \
      --conditionsTag 'CONDBR2-BLKPA-2023-05' \
      --geometryVersion 'ATLAS-R3S-2021-03-02-00' \
      --runNumber '451569' \
      --athenaopts="--stdcmalloc" \
      --valgrind "True" \
      --valgrindDefaultOpts "False" \
      --valgrindExtraOpts="${valgrindOpts}" \
      --steering 'doRAWtoALL';

    # Get the exit code
    echo $? > __exitcode;

}

run_r2a_mc23() {
    # See which analysis we are running...
    DATADIR="/data/atlaspmb/spot-job-inputs/mc23_13p6TeV"
    # Parse the inputs
    NTHREADS=${1}
    NEVENTS=${2}
    valgrindOpts=${3}
    # Run the job
    export TRF_ECHO=1;
    ATHENA_CORE_NUMBER=${NTHREADS} \
    Reco_tf.py \
      --CA 'default:True' \
      --inputHITSFile "${DATADIR}/HITS/mc23_13p6TeV.601237.PhPy8EG_A14_ttbar_hdamp258p75_allhad.merge.HITS.e8514_e8528_s4159_s4114/HITS.34124871._003416.pool.root.1" \
      --inputRDO_BKGFile "${DATADIR}/RDO/mc23_13p6TeV.900149.PG_single_nu_Pt50.merge.RDO.e8514_e8528_s4153_d1879_d1880/RDO.33837536._002942.pool.root.1" \
      --outputAODFile 'myAOD.pool.root' \
      --perfmon 'fullmonmt' \
      --maxEvents ${NEVENTS} \
      --multithreaded='True' \
      --preInclude 'all:Campaigns.MC23c' \
      --postInclude 'default:PyJobTransforms.UseFrontier' \
      --skipEvents '0' \
      --autoConfiguration 'everything' \
      --conditionsTag 'default:OFLCOND-MC23-SDR-RUN3-05' \
      --geometryVersion 'default:ATLAS-R3S-2021-03-02-00' \
      --runNumber '601237' \
      --digiSeedOffset1 '232' \
      --digiSeedOffset2 '232' \
      --AMITag 'r14799' \
      --athenaopts="--stdcmalloc" \
      --valgrind "True" \
      --valgrindDefaultOpts "False" \
      --valgrindExtraOpts="${valgrindOpts}" \
      --steering 'doOverlay' 'doRDO_TRIG'  > __log.txt 2>&1;

    # Get the exit code
    echo $? > __exitcode;

}
# ttbar full-chain MC
run_mcttbar(){
    
    # Parse the inputs
    NTHREADS=${1}
    NEVENTS=${2}
    valgrindOpts=${3}

    # Run the job
    export TRF_ECHO=1;
    Sim_tf.py \
      --CA "True" \
      --perfmon "none" \
      --inputEVNTFile "/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/CampaignInputs/mc23/EVNT/mc23_13p6TeV.601229.PhPy8EG_A14_ttbar_hdamp258p75_SingleLep.evgen.EVNT.e8514/EVNT.32288062._002040.pool.root.1" \
      --maxEvents ${NEVENTS} \
      --outputHITSFile "myHITS.pool.root" \
      --AMIConfig "s4006" \
      --conditionsTag "default:OFLCOND-MC21-SDR-RUN3-07" \
      --geometryVersion "default:ATLAS-R3S-2021-03-02-00" \
      --jobNumber "1" \
      --athenaopts="--stdcmalloc" \
      --valgrind "True" \
      --valgrindDefaultOpts "False" \
      --valgrindExtraOpts="${valgrindOpts}";

   # Get the exit code
    echo $? > __exitcode;

}

# The function that sets up valgrind related bits
setup_valgrind() {

  # Set some useful valgrind options
  source $(which valgrind-atlas-opts.sh)
  echo "=================" > settings_valgrind.txt
  echo "VALGRIND SETTINGS" >> settings_valgrind.txt
  echo "=================" >> settings_valgrind.txt
  echo "LIB  : ${VALGRIND_LIB}" >> settings_valgrind.txt
  echo "OPTS : ${VALGRIND_OPTS}" >> settings_valgrind.txt
  echo "=================" >> settings_valgrind.txt

}

# Define and execute the test
execute() {  
    # Define test parameters
    JOBNAME=${1};
    JOBRELEASE=${2};
    JOBPLATFORM=${3};
    ANALYSIS=${4};
    echo "${JOBNAME} - ${JOBRELEASE} - ${JOBPLATFORM} - ${ANALYSIS}"

    # Define the top-level workdir
    WORKDIR="/data/atlaspmb/athenamt-perfmonmt-jobs/valgrind";

    # Create the rundir
    RUNDIR="${WORKDIR}/${ANALYSIS}/${JOBNAME}/${JOBRELEASE}/${JOBPLATFORM}";
    if [[ ! -d ${RUNDIR} ]]; then
        mkdir -p ${RUNDIR};
    fi

    # Go to the main rundir
    echo "Using ${RUNDIR} as the rundir...";
    cd "${RUNDIR}";

 #   export LD_PRELOAD="${TCMALLOCDIR}/libtcmalloc_minimal.so:${ATLASMKLLIBDIR_PRELOAD}/libimf.so";

    ulimit -n 4096
    # Setup the latest Athena - job runs once per day at a fixed time
    lsetup "asetup Athena,${JOBRELEASE},${JOBPLATFORM//-/,},latest";

    # Check the currently nightly tag
    nightly=`echo "${Athena_DIR##*/${JOBRELEASE}_Athena_${JOBPLATFORM}/}"`;
    nightly=`echo "${nightly%%/Athena/*}"`;
    setup_valgrind

    # See which analysis we are running...
    local valgrindOpts=""
    if [[ "${ANALYSIS}" == "memcheck" ]]; then
       valgrindOpts="${VALGRIND_OPTS},--show-possibly-lost=no,--smc-check=all,--tool=memcheck,--leak-check=full,--num-callers=30,--log-file=valgrind.%p.%n.out,--track-origins=yes"
    elif [[ "${ANALYSIS}" == "massif" ]]; then
       valgrindOpts="${VALGRIND_OPTS},--tool=massif,--pages-as-heap=yes,--threshold=0.01,--detailed-freq=1,--log-file=valgrind.out"
    else
       echo "Unknown analysis ${ANALYSIS} in run_valgrind - running memcheck ..."
       valgrindOpts="${VALGRIND_OPTS},--show-possibly-lost=no,--smc-check=all,--tool=memcheck,--leak-check=full,--num-callers=30,--log-file=valgrind.%p.%n.out,--track-origins=yes"
    fi
    
    # Check if it exists already
    if [[ -d "${nightly}" ]]; then
        echo "Directory for ${nightly} already exists, nothing to do."
        return 0;
    fi

    # Now setup the run directory
    mkdir -p "${nightly}"; 
    cd "${nightly}";

    # Let's start
    touch __start;


    if [[ "${JOBNAME}" == "rawtoall_data23_mt8" ]]; then
        run_r2a_data23 1 10 "${valgrindOpts}"; 
    elif [[ "${JOBNAME}" == "ttbar_fullsim_mt8" ]]; then
         run_mcttbar 1 10 "${valgrindOpts}";
    elif [[ "${JOBNAME}" == "reco_mc23_mt8" ]]; then
          run_r2a_mc23 1 10 "${valgrindOpts}";
    else
        echo "Unknown job ${JOBNAME}, quitting..."
        return 0
    fi


    # Let's extract the transform command to be used on the webpage
    echo "#!/bin/bash" > __command.txt;
    if [[ -f "env.txt" ]]; then
        echo "export $( grep "ATHENA_CORE_NUMBER" env.txt )" >> __command.txt;
    fi

    # All done
    touch __done;

    rsync -avuz "${RUNDIR}/${nightly}" aiatlasbm001.cern.ch:"/${RUNDIR}"; 
    # Go back to rundir
    cd "${RUNDIR}";

}

# Define the main function
main() {
    # Setup environment
    source ~/.bashrc;
    source ~/.bash_profile;

    pidof -o %PPID -x $0 >/dev/null && echo "WARNING: Script ${0} already running, nothing to do..." && exit 0

    # These are the standard jobs
    JOBS=( "main--dev4LCG x86_64-el9-gcc13-opt rawtoall_data23_mt8 memcheck" \
               'main--dev4LCG x86_64-el9-gcc13-opt ttbar_fullsim_mt8 memcheck')

    # Loop over all defined jobs
    for job in "${JOBS[@]}"
    do
        # Parse the inputs: release platform name
        vals=(${job})
        jobrelease="${vals[0]}"
        jobplatform="${vals[1]}"
        jobname="${vals[2]}"
        analysis="${vals[3]}"
       # Match the release w/ the user input
        execute "${jobname}" "${jobrelease}" "${jobplatform}" "${analysis}"
    done

}

# Execute the main function
main
