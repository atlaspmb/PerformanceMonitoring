#!/usr/bin/env python
import argparse

# A few parameters
num_callers = 30

# Check calls for terminating
call_list = [ 'ApplicationMgr::initialize',
              'ApplicationMgr::executeRun',
              'ApplicationMgr::finalize',
              'ApplicationMgr::start',
              'ApplicationMgr::stop',
              'Gaudi::Algorithm::sysInitialize',
              'Gaudi::Algorithm::sysExecute',
              'Gaudi::Algorithm::sysFinalize',
              'AthenaEventLoopMgr::executeEvent',
              'AthenaEventLoopMgr::executeRun',
              'AthenaHiveEventLoopMgr::executeEvent',
              'AthenaHiveEventLoopMgr::executeRun',
              'PileUpEventLoopMgr::executeEvent',
              'PileUpEventLoopMgr::executeRun',
              'FastCall',
              'AlgTask<tbb::task>::execute()',
              'start_thread',
              '(below main)' ]

# Helper to decide if we should break
def break_on_line(line):
    return any([ call in line for call in call_list])

# Print fill command
def print_full_command():
    with open('memcheck-valgrind-inclusive.sh') as f:
        for line in f:
            print(line.strip())

# Generic search function
report_types = {'uninit'  : 'Uninitialisation',
                'invalid' : 'Invalid Read/Write'}
search_strs  = {'uninit'  : [['Conditional jump or move depends on uninitialised value(s)'], 'Uninitialised value was created by'],
                'invalid' : [['Invalid read','Invalid write'],'Address']}

def print_common_values(in_file, only_execute, report_type):
    total_reported = 0
    with open(in_file) as f:
        all_lines = f.readlines() # loads all into memory
        for idx,line in enumerate(all_lines):
            if any([ pattern in line for pattern in search_strs[report_type][0] ]):
                if only_execute and not any('execute' in stack for stack in all_lines[idx:idx+num_callers+1]):
                    continue
                print('')
                print('-'*50)
                print('{}===>>> {} # {} <<<==='.format(' '*10,report_types[report_type],total_reported+1))
                print('-'*50)
                skipLine=False
                for jdx,ll in enumerate(all_lines[idx:idx+100]):
                    refined = ll.split('==')[2].strip()
                    if break_on_line(ll):
                        skipLine=True
                    if not skipLine or search_strs[report_type][1] in ll:
                        if search_strs[report_type][1] in ll:
                            upper_index_offset = num_callers if 'Address' not in refined else 1
                            for lll in all_lines[idx+jdx+1:idx+jdx+upper_index_offset+1]:
                                if break_on_line(lll):
                                    break
                                refined2 = lll.split('==')[2].strip()
                                print('{}{}'.format((' ' if ('at' in ll or 'by' in ll) else ''),refined2))
                            break
                        else:
                            print('{}{}'.format((' ' if ('at' in ll or 'by' in ll) else ''),refined))
                print('-'*50)
                print('')
                total_reported += 1
    print('*'*125)
    print('Total number of reported {}s  = {}'.format(report_types[report_type],total_reported))
    print('*'*125)

# Printing definitely lost leaks
''' Threshold in b '''
def print_leaks(in_file, only_execute = True, leak_type = 'definitely', threshold = 0):
    if 'reachable' in leak_type:
        leak_type = f"still {leak_type}"
    else:
        leak_type = f"{leak_type} lost"
    total_reported = 0
    with open(in_file) as f:
        all_lines = f.readlines() # loads all into memory
        for idx,line in enumerate(all_lines):
            if f"{leak_type}" in line:
                memleaksize_str = line.split()[1]
                try:
                    memleaksize_f = float(memleaksize_str.replace(',',''))
                    if memleaksize_f > threshold:
                        if only_execute and not any('execute' in stack for stack in all_lines[idx:idx+31]):
                            continue
                        print('')
                        print('-'*50)
                        print('{}===>>> Leak # {} <<<==='.format(' '*12,total_reported+1))
                        print('-'*50)
                        for ll in all_lines[idx:idx+num_callers+1]:
                            tmp = ll.split('==')[2].strip().split(' (in')
                            refined = tmp[0]
                            if len(tmp) > 1:
                                refined += ' (in ' + tmp[1].split('/')[-1]
                            print('{}{}'.format(('  ' if f"{leak_type}" not in ll else ''),refined))
                            if break_on_line(ll):
                                break
                        print('-'*50)
                        print('')
                        total_reported += 1
                except:
                    continue
    print('*'*125)
    print('Total number of reported leaks  = %i'%(total_reported))
    print('*'*125)

# Main
if '__main__' in __name__:

    # Parser
    parser = argparse.ArgumentParser(description = 'Valgrind memcheck report analyzer for Athena reports',
                                     add_help=True)

    parser.add_argument('-i', '--input', action = 'store', dest = 'input_file', required = True,
                        help = 'name of the input file that contains the valgrind memcheck report')
    parser.add_argument('-t', '--type', action = 'store', dest = 'analysis_type', default = 'ALL',
                        choices = ['ALL' ,'LEAK', 'UNINIT', 'INVALID'],
                        help = 'analysis type')
    parser.add_argument('-x', '--leak-type', action = 'store', dest = 'leak_type', default = 'DEFINITELY',
                        choices = ['DEFINITELY', 'POSSIBLY', 'REACHABLE'],
                        help = 'leak type')
    parser.add_argument('-e', '--extra', action = 'store_true', dest = 'print_command',
                        help = 'print generation command (expert-only!)')
    parser.add_argument('-l', '--loop', action = 'store_true', dest = 'only_execute',
                        help = 'print errors in the loop-only')
    parser.add_argument('-v', '--version', action = 'version', version = '%(prog)s 0.1')

    args = parser.parse_args()

    # Set args
    in_file = args.input_file
    only_execute = args.only_execute
    print_command = args.print_command
    analysis_type = args.analysis_type
    leak_type = args.leak_type.lower()

    # Report
    print('*'*125)
    print('Provided input file is = {}'.format(in_file))
    print('*'*125)
    if print_command:
        print('')
        print('='*125)
        print('{}<<== Full Command ==>>'.format(' '*50))
        print('='*125)
        print_full_command()
        print('='*125)
    if analysis_type == 'ALL' or analysis_type == 'LEAK':
        print('')
        print('='*125)
        print('{}<<== Leak Report ==>>'.format(' '*50))
        print('='*125)
        print_leaks(in_file, only_execute, leak_type)
    if analysis_type == 'ALL' or analysis_type == 'UNINIT':
        print('')
        print('='*125)
        print('{}<<== Uninitialisation Report ==>>'.format(' '*50))
        print('='*125)
        print_common_values(in_file, only_execute, 'uninit')
    if analysis_type == 'ALL' or analysis_type == 'INVALID':
        print('')
        print('='*125)
        print('{}<<== Invalid Read/Write Report ==>>'.format(' '*50))
        print('='*125)
        print_common_values(in_file, only_execute, 'invalid')

