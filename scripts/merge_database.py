#!/usr/bin/env python3

from utils import in_area, db_area, web_area, jobs
import glob
import os
import pmbDB as mydb
import time
import subprocess

def dangling_file(file_name):
    creation_time = os.path.getctime(file_name)
    return ((time.time() - creation_time) > 3600000)

if '__main__' in __name__:

    to_be_deleted = []
 
    for job in jobs:
        # Check the arguments and unpack
        if job is None or len(job) != 3:
            continue 
        (job_name, _ , _) = job

        # Build the pattern
        pattern = '{}/__d*__{}__run.db'.format(in_area, job_name)
    
        # Search for the files
        file_list=glob.glob(pattern)
    
        # Ignore dangling files
        for f in file_list[:]:
            if dangling_file(f):
                print('Removing file due to creation time {}'.format(f))
                file_list.remove(f)
    
        # Print some information
        print('Found {} files for job {}'.format(len(file_list),job_name))
    
        if not file_list: # Nothing to be done
            print('No database files found for job {}'.format(job_name))
            continue
    
        # Check if baseline database exists
        db_file_name = '{}/{}.db'.format(db_area, job_name)
    
        if os.path.exists(db_file_name):
            print('Merging results for job {} into {}'.format(job_name,db_file_name))
            x=mydb.pmbDB('{}'.format(db_file_name))
            x.merge_db_files(file_list)
        else:
            print('Copying and Merging results for job {} into {}'.format(job_name,db_file_name))
            os.system('cp {} {}'.format(file_list[0],db_file_name))
            if len(file_list) > 1:
                x=mydb.pmbDB('{}'.format(db_file_name))
                x.merge_db_files(file_list[1:])

        # Remember to delete the files
        for f in file_list:
            to_be_deleted.append(f)
    
    # Delete files
    for f in to_be_deleted:
        print('Deleting {}'.format(f))
        os.remove(f)
